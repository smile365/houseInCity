package com.five.service;

import com.five.common.DataGridView;
import com.five.domain.News;
import com.five.vo.NewsVo;

import java.util.List;

public interface NewsService {

    //查询所有的新闻
    List<News> queryAllNews(NewsVo newsVo);

    //添加新闻
    DataGridView addNews(NewsVo newsVo);

    //修改新闻
    DataGridView updateNews(NewsVo newsVo);

    //删除(恢复)新闻
    DataGridView deleteNews(NewsVo newsVo);

    //根据ID查询新闻
    News queryNewById(NewsVo newsVo);

}
