package com.five.service;

import com.five.common.DataGridView;
import com.five.domain.City;
import com.five.vo.CityVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CityService {

    //查询城市
    List<City> queryAllCity(CityVo cityVo);

    //查询城市
    City queryCityByName(CityVo cityVo);

    //导航栏城市查询
    List<City> queryCityByUse(CityVo cityVo);

    //添加城市
    DataGridView addCity(CityVo cityVo);

    //修改城市信息
    DataGridView updateCity(CityVo cityVo);

    //删除(恢复)城市信息
    DataGridView deleteCity(CityVo cityVo);
}
