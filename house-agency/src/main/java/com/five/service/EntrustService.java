package com.five.service;

import com.five.common.DataGridView;
import com.five.domain.Entrust;
import com.five.vo.EntrustVo;

import java.util.List;

public interface EntrustService {

    //查询委托信息
    List<Entrust> queryEntrust(EntrustVo entrustVo);

    //删除委托信息
    DataGridView deleteEntrust(EntrustVo entrustVo);

    //添加委托
    DataGridView addEntrust(EntrustVo entrustVo);

    //查询该用户的委托
    List<Entrust> queryEntrustByUser(EntrustVo entrustVo);

    DataGridView queryEntrustNumber(Entrust entrust);
}
