package com.five.service.impl;

import com.five.common.DataGridView;
import com.five.common.UserUtil;
import com.five.common.file.AppFileUtils;
import com.five.common.file.SysConstast;
import com.five.domain.News;
import com.five.mapper.NewsMapper;
import com.five.service.NewsService;
import com.five.vo.NewsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class NewsServiceImpl implements NewsService {

    @Autowired
    private NewsMapper newsMapper;

    @Override
    public List<News> queryAllNews(NewsVo newsVo) {
        List<News> data = newsMapper.queryAllNews(newsVo);
        return data;
    }

    //添加新闻信息
    @Override
    public DataGridView addNews(NewsVo newsVo){
        Date date = new Date();
        DataGridView dataGridView = new DataGridView();
        newsVo.setAvailable(1);
        newsVo.setActer(UserUtil.getUser().getId());
        newsVo.setTime(date);
        String filePath = AppFileUtils.updateFileName(newsVo.getImage(), SysConstast.FILE_UPLOAD_TEMP);
        newsVo.setImage(filePath);
        int row =newsMapper.addNews(newsVo);
        if(row == 0){
            dataGridView.setCode(500);
            dataGridView.setMsg("添加失败，请重新添加！！");
        }else {
            dataGridView.setCode(200);
            dataGridView.setMsg("添加成功！！");
        }
        return dataGridView;
    }

    //修改新闻信息
    @Override
    public DataGridView updateNews(NewsVo newsVo){
        DataGridView dataGridView = new DataGridView();
        String filePath = AppFileUtils.updateFileName(newsVo.getImage(), SysConstast.FILE_UPLOAD_TEMP);
        newsVo.setImage(filePath);
        int row = newsMapper.updateNews(newsVo);
        if(row == 1){
            dataGridView.setCode(200);
            dataGridView.setMsg("修改成功！！");
        }else {
            dataGridView.setCode(500);
            dataGridView.setMsg("修改失败！！");
        }
        return dataGridView;
    }

    //删除新闻信息
    @Override
    public DataGridView deleteNews(NewsVo newsVo){
        DataGridView dataGridView = new DataGridView();
        int row = newsMapper.deleteNews(newsVo);
        if(row == 1){
            AppFileUtils.deleteFileUsePath(newsVo.getImage());
            dataGridView.setMsg("删除成功");
            dataGridView.setCode(200);
        }else{
            dataGridView.setCode(500);
            dataGridView.setMsg("删除失败");
        }
        return dataGridView;
    }

    @Override
    public News queryNewById(NewsVo newsVo) {
        return newsMapper.queryNewById(newsVo);
    }
}
