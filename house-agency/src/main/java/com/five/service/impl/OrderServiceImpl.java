package com.five.service.impl;

import com.five.common.DataGridView;
import com.five.common.UUIDUtil;
import com.five.common.UserUtil;
import com.five.common.exception.MyException;
import com.five.domain.BaseEntity;
import com.five.domain.House;
import com.five.domain.Order;
import com.five.mapper.HouseMapper;
import com.five.mapper.OrderMapper;
import com.five.service.OrderService;
import com.five.vo.OrderVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private HouseMapper houseMapper;

    @Override
    public List<Order> queryAllOrder(OrderVo orderVo) {
        List<Order> data = orderMapper.queryAllOrder(orderVo);
        return data;
    }

    //添加订单
    @Override
    @Transactional
    public String addOrder(OrderVo orderVo) throws Exception {
        //设置订单需要的参数
        orderVo.setId(UUIDUtil.getUUID());
        orderVo.setoId(UserUtil.getUser().getId());
        orderVo.setTime(new Date());
        //获取房屋价格，计算提成
        House house = new House();
        house.setId(orderVo.gethId());
        House house1 = houseMapper.queryHouseById(house);
        if (house1.getSaId().equals(UserUtil.getUser().getId())) throw new MyException("不能购买/租凭自己的房屋");
        if(house1.getStatus()!=1) throw new Exception();
        Double price = house1.getPrice();
        orderVo.setMoney(price);
        BigDecimal num1 = new BigDecimal(price);
        if(house1.getType()==1||house1.getType()==2){
            BigDecimal num2 = new BigDecimal(10000);
            BigDecimal num3 = new BigDecimal(0.01);
            num1 = num1.multiply(num2);
            num1 = num1.multiply(num3);
            Double df = num1.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
            orderVo.setCommission(df);
        }else if(house1.getType()==3){
            BigDecimal num3 = new BigDecimal(0.05);
            num1 = num1.multiply(num3);
            Double df = num1.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
            orderVo.setCommission(df);
        }
        orderMapper.addOrder(orderVo);
        house.setStatus(2);
        houseMapper.cancelHouse(house);
        return orderVo.getId();
    }

    //查看订单
    @Override
    public DataGridView lookOrder(OrderVo orderVo){
        orderVo.setoId(UserUtil.getUser().getId());
        return new DataGridView(200,"",orderMapper.lookOrder(orderVo));
    }

    //取消订单
    @Override
    @Transactional
    public void cancelOrder(OrderVo orderVo) throws Exception {
        Order order = orderMapper.lookOrder(orderVo);
        if(order.getStatus()!=1) throw new Exception();
        House house = new House();
        house.setId(order.gethId());
        house.setStatus(3);
        houseMapper.cancelHouse(house);
        orderVo.setStatus(3);
        orderMapper.updateOrder(orderVo);
    }

    //支付订单
    @Override
    @Transactional
    public void payOrder(String id,Integer inputnum) throws Exception {
        Order order = new Order();
        order.setId(id);
        Order order1 = orderMapper.lookOrder(order);
        if(order1.getStatus()!=1) throw new Exception();
        order.setMoney(order1.getHprice()*inputnum);
        order.setCommission(order1.getCommission()*inputnum);
        order.setStatus(2);
        orderMapper.updateOrder(order);
    }

    //支出情况
    @Override
    public List<Order> queryAllOrderByBid(OrderVo orderVo) {
        orderVo.setoId(UserUtil.getUser().getId());
        List<Order> data = orderMapper.queryAllOrderByOther(orderVo);
        return data;
    }

    //收入情况
    @Override
    public List<Order> queryAllOrderBySaid(OrderVo orderVo) {
        orderVo.setSaId(UserUtil.getUser().getId());
        List<Order> data = orderMapper.queryAllOrderByOther(orderVo);
        return data;
    }

    //统计个人销售额
    @Override
    public List<Double> loadOrderYear(Integer year) {
        String staff = UserUtil.getUser().getId();
        return orderMapper.loadOrderYear(year,null, staff,null);
    }

    //统计城市员工销售额
    @Override
    public List<BaseEntity> LoadStaffCity(Integer city) {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        return orderMapper.LoadStaffCity(year,city);
    }

    //统计城市销售额
    @Override
    public List<Double> loadCityYear(Integer year, Integer city) {
        return  orderMapper.loadOrderYear(year,city,null,null);
    }

    //统计员工每月销售额
    @Override
    public List<Double> loadStaffYear(Integer year, String staff) {
        return  orderMapper.loadOrderYear(year,null, staff,null);
    }

    //统计订单总数
    @Override
    public DataGridView countOrder(Order order) {
        Long count = orderMapper.countOrder(UserUtil.getUser().getId(),order.getStatus());
        return new DataGridView(count,"");
    }

    //统计新房销售额
    @Override
    public List<Double> loadNewOrder(Integer year) {
        return orderMapper.loadOrderYear(year,null, null,"00000000000000000000000000000000");
    }
}
