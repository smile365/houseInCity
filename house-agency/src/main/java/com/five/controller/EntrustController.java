package com.five.controller;

import com.five.common.DataGridView;
import com.five.domain.Entrust;
import com.five.service.EntrustService;
import com.five.vo.EntrustVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/admin/entrust")
public class EntrustController {

    @Autowired
    EntrustService entrustService;

    @RequestMapping("/queryEntrust")
    @PreAuthorize("hasPermission('/admin/entrust/toSellEntrust',null)")
    public DataGridView queryEntrust(EntrustVo entrustVo){
        entrustVo.setType(2);
        Page<Object> page = PageHelper.startPage(entrustVo.getPage(), entrustVo.getLimit());
        List<Entrust> data = entrustService.queryEntrust(entrustVo);
        return new DataGridView(page.getTotal(), data);
    }

    @RequestMapping("/queryLeaEntrust")
    @PreAuthorize("hasPermission('/admin/entrust/toLeaseEntrust', null)")
    public DataGridView queryLeaEntrust(EntrustVo entrustVo){
        entrustVo.setType(3);
        Page<Object> page = PageHelper.startPage(entrustVo.getPage(), entrustVo.getLimit());
        List<Entrust> data = entrustService.queryEntrust(entrustVo);
        return new DataGridView(page.getTotal(), data);
    }

    @RequestMapping("/deleteEntrust")
    @PreAuthorize("hasPermission('/admin/entrust/deleteEntrust', 'entrust:delete')")
    public DataGridView deleteEntrust(EntrustVo entrustVo){
        return entrustService.deleteEntrust(entrustVo);
    }


}
