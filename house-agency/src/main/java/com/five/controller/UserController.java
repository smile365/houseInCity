package com.five.controller;

import com.five.common.DataGridView;
import com.five.common.exception.MyException;
import com.five.domain.SysUser;
import com.five.service.UserService;
import com.five.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/admin/user")
public class UserController {
    @Autowired
    private UserService userService;

    //返回员工列表
    @RequestMapping("/loadAllStaff")
    @PreAuthorize("hasPermission('/admin/user/staffList',null)")
    public DataGridView loadAllStaff(UserVo userVo) {
        userVo.setType(1);
        return userService.queryAllUser(userVo);
    }

    //添加员工
    @RequestMapping("/addStaff")
    @PreAuthorize("hasPermission('/admin/user/addStaff','staff:add')")
    public DataGridView addStaff(UserVo userVo){
        try {
            userService.addStaff(userVo);
            return DataGridView.ADD_SUCCESS;
        }
        catch (MyException me){
            return DataGridView.ADD_ERROR2;
        }
        catch (Exception e){
            return DataGridView.ADD_ERROR;
        }
    }

    //修改员工
    @RequestMapping("/updateStaff")
    @PreAuthorize("hasPermission('/admin/user/updateStaff','staff:update')")
    public DataGridView updateStaff(UserVo userVo){
        try {
            userService.updateStaff(userVo);
            return DataGridView.UPDATE_SUCCESS;
        }
        catch (Exception e){
            return DataGridView.UPDATE_ERROR;
        }
    }

    //加载用户管理的分配角色的数据
    @RequestMapping("/initStaffRole")
    @PreAuthorize("hasPermission('/admin/user/saveStaffRole','staff:save')")
    public DataGridView initStaffRole(UserVo userVo) {
        return this.userService.queryStaffRole(userVo.getId());
    }

    //保存用户和角色的关系
    @RequestMapping("/saveStaffRole")
    @PreAuthorize("hasPermission('/admin/user/saveStaffRole','staff:save')")
    public DataGridView saveStaffRole(UserVo userVo) {
        try {
            userService.saveStaffRole(userVo);
            return DataGridView.DISPATCH_SUCCESS;
        } catch (Exception e) {
            return DataGridView.DISPATCH_ERROR;
        }
    }

    //密码重置
    @RequestMapping("/resetPwd")
    @PreAuthorize("hasPermission('/admin/user/resetPwd','staff:reset')")
    public DataGridView resetUserPwd(UserVo userVo) {
        try {
            userService.resetPwd(userVo.getId());
            return DataGridView.RESET_SUCCESS;
        } catch (Exception e) {
            return DataGridView.RESET_ERROR;
        }
    }

    //返回顾客列表
    @RequestMapping("/loadAllCustomer")
    @PreAuthorize("hasPermission('/admin/user/customerList',null)")
    public DataGridView loadAllCustomer(UserVo userVo) {
        userVo.setType(0);
        return userService.queryAllUser(userVo);
    }

    //禁用与解禁用户
    @RequestMapping("/banCustomer")
    @PreAuthorize("hasPermission('/admin/user/banCustomer','customer:ban')")
    public DataGridView banCustomer(String id, Integer available){
        try {
            userService.banCustomer(id,available);
            return DataGridView.BAN_SUCCESS;
        }catch (Exception e){
            return DataGridView.BAN_ERROR;
        }
    }

    //修改密码
    @RequestMapping("/updatePwd")
    @PreAuthorize("hasPermission('/admin',null)")
    public DataGridView updatePwd(String oldpassword, String newpassword){
        try {
            userService.updatePwd(oldpassword,newpassword);
            return DataGridView.UPDATE_SUCCESS;
        }
        catch (MyException ue){
            return DataGridView.UPDATE_PWD_ERROR;
        }
        catch (Exception e){
            return DataGridView.UPDATE_ERROR;
        }
    }

    //返回个人资料
    @RequestMapping("/loadUser")
    @PreAuthorize("hasPermission('/admin',null)")
    public DataGridView loadUser() {
        return userService.queryUser();
    }

    //修改个人资料
    @RequestMapping("/changeUser")
    @PreAuthorize("hasPermission('/admin',null)")
    public DataGridView updateUser(UserVo userVo) {
        try {
            userService.updateInfo(userVo);
            return DataGridView.UPDATE_SUCCESS;
        }
        catch (Exception e){
            return DataGridView.UPDATE_ERROR;
        }
    }

    //可用员工
    @RequestMapping("/getStaff")
    @PreAuthorize("hasPermission('/admin',null)")
    public DataGridView getStaff(){
        List<SysUser> data = userService.queryStaffByUse();
        return new DataGridView(data);
    }

}
