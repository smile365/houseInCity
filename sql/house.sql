/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 5.5.36 : Database - house
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`house` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `house`;

/*Table structure for table `city` */

DROP TABLE IF EXISTS `city`;

CREATE TABLE `city` (
  `c_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `c_name` varchar(50) NOT NULL COMMENT '城市',
  `c_available` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否可见',
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `city` */

insert  into `city`(`c_id`,`c_name`,`c_available`) values (1,'北京',1),(2,'杭州',1),(3,'太湖',1),(4,'上海',1),(5,'天津',1),(6,'武汉',0);

/*Table structure for table `decoration` */

DROP TABLE IF EXISTS `decoration`;

CREATE TABLE `decoration` (
  `d_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `d_style` varchar(50) NOT NULL COMMENT '装修风格',
  `d_available` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否可见',
  PRIMARY KEY (`d_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `decoration` */

insert  into `decoration`(`d_id`,`d_style`,`d_available`) values (1,'毛坯',1),(2,'简易装修',1),(3,'中档装修',1),(4,'高档装修',1),(5,'豪华装修',1);

/*Table structure for table `entrust` */

DROP TABLE IF EXISTS `entrust`;

CREATE TABLE `entrust` (
  `e_id` varchar(50) NOT NULL COMMENT 'id',
  `e_name` varchar(50) NOT NULL COMMENT '姓名',
  `e_phone` varchar(20) NOT NULL COMMENT '手机号',
  `e_city` int(11) NOT NULL COMMENT '城市',
  `e_local` varchar(50) NOT NULL COMMENT '地址',
  `e_time` datetime NOT NULL COMMENT '委托时间',
  `e_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '委托类型',
  PRIMARY KEY (`e_id`),
  KEY `FK_name_sys_user` (`e_name`),
  KEY `FK_city_city` (`e_city`),
  CONSTRAINT `FK_city_city` FOREIGN KEY (`e_city`) REFERENCES `city` (`c_id`),
  CONSTRAINT `FK_name_sys_user` FOREIGN KEY (`e_name`) REFERENCES `sys_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `entrust` */

insert  into `entrust`(`e_id`,`e_name`,`e_phone`,`e_city`,`e_local`,`e_time`,`e_type`) values ('8d5ecf5482d44f20b87a03bf487b098b','4f6295e7d6b542d8a81fc6db60968134','56474648888',1,'大陆桥','2021-06-10 00:41:58',2),('d0db91a2f98145b188fa40cc8bb2ee5f','ebc257ec06224b61bb1be042ea123821','44444444444',4,'静安','2021-06-10 00:29:41',3),('fabed3e469bf436e9220f7984f568ff3','ebc257ec06224b61bb1be042ea123821','34243333333',1,'颐和园大道','2021-06-10 00:28:03',2);

/*Table structure for table `houses` */

DROP TABLE IF EXISTS `houses`;

CREATE TABLE `houses` (
  `h_id` varchar(50) NOT NULL COMMENT 'id',
  `h_title` varchar(20) NOT NULL COMMENT '标题',
  `h_city` int(11) NOT NULL COMMENT '城市',
  `h_quarters` varchar(20) NOT NULL COMMENT '楼盘小区',
  `h_local` varchar(30) NOT NULL COMMENT '地址',
  `h_price` double NOT NULL COMMENT '价格',
  `h_area` double NOT NULL COMMENT '面积',
  `h_higth` int(11) NOT NULL COMMENT '楼层',
  `h_allhigth` int(11) NOT NULL COMMENT '总楼层',
  `h_characteristic` varchar(30) NOT NULL COMMENT '房屋特色',
  `h_sale` int(11) DEFAULT NULL COMMENT '出售类型',
  `h_style` int(11) DEFAULT NULL COMMENT '装修风格',
  `h_details` text COMMENT '详细内容',
  `h_type` tinyint(4) NOT NULL COMMENT '类型(1新房2二手房3租房)',
  `h_agent` varchar(50) NOT NULL COMMENT '经济人',
  `h_salename` varchar(50) DEFAULT NULL COMMENT '卖家',
  `h_status` tinyint(4) NOT NULL COMMENT '状态',
  `h_time` datetime NOT NULL COMMENT '时间',
  `h_img` varchar(255) DEFAULT NULL COMMENT '图片',
  PRIMARY KEY (`h_id`),
  KEY `FK_hcity_city` (`h_city`),
  KEY `FK_hsale_sale` (`h_sale`),
  KEY `FK_hdecoration_decoration` (`h_style`),
  KEY `FK_hagent_sys_user` (`h_agent`),
  KEY `FK_hsalename_sys_user` (`h_salename`),
  CONSTRAINT `FK_hagent_sys_user` FOREIGN KEY (`h_agent`) REFERENCES `sys_user` (`id`),
  CONSTRAINT `FK_hcity_city` FOREIGN KEY (`h_city`) REFERENCES `city` (`c_id`),
  CONSTRAINT `FK_hdecoration_decoration` FOREIGN KEY (`h_style`) REFERENCES `decoration` (`d_id`),
  CONSTRAINT `FK_hsalename_sys_user` FOREIGN KEY (`h_salename`) REFERENCES `sys_user` (`id`),
  CONSTRAINT `FK_hsale_sale` FOREIGN KEY (`h_sale`) REFERENCES `sale` (`s_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `houses` */

insert  into `houses`(`h_id`,`h_title`,`h_city`,`h_quarters`,`h_local`,`h_price`,`h_area`,`h_higth`,`h_allhigth`,`h_characteristic`,`h_sale`,`h_style`,`h_details`,`h_type`,`h_agent`,`h_salename`,`h_status`,`h_time`,`h_img`) values ('03a7801717634091b477faf64dcbe0f3','人民路商铺',4,'无','人民路21号',210,80,1,2,'临街',1,2,'临街商铺',1,'881817b4e97642bca8a179d00b0dc038','00000000000000000000000000000000',2,'2021-06-09 23:33:55','2021-06-09/202106092333416133761.jpeg'),('0ab90cabb7a74429a7009b4caee5fff0','天津桥',5,'众创','繁华里8号',310,120,16,21,'交通方便',2,4,'w',1,'2','00000000000000000000000000000000',2,'2021-06-09 23:37:58','2021-06-09/202106092337544125025.jpeg'),('0baa1d2b1898402eb480c19a88e28dd0','杭州闯关',2,'nb','谷闯关',3800,89,3,17,'大',NULL,NULL,'21',3,'881817b4e97642bca8a179d00b0dc038','4f6295e7d6b542d8a81fc6db60968134',1,'2021-06-09 22:41:18','2021-06-09/202106092241144861493.jpeg'),('0c1226ca592a4ae08b7b24fc05056a9c','中南海复式',1,'梅三','中南海',300,120,2,10,'区位好',1,3,'豪华装修，巅峰住宅',2,'2','e5da420263d94f5c8581b494d3f2e998',3,'2021-06-09 22:28:15','2021-06-09/202106092227504968983.jpeg'),('17f62278ef6d42f88a3d5a2683104c0d','步行街',1,'王府井','王府井98号',360,100,1,10,'新',1,3,'无',1,'2','00000000000000000000000000000000',1,'2021-06-09 23:36:25','2021-06-09/202106092336190624418.jpeg'),('24f4295e97514360b592d6e3dcc5b09c','虹桥机场',4,'机场路小区','虹桥',180,105,2,9,'交通方便',2,3,'靠近虹桥国际机场',2,'881817b4e97642bca8a179d00b0dc038','4556c046f8774d55b249b3cba0efff60',1,'2021-06-09 22:59:29','2021-06-09/202106092259090051025.jpeg'),('329d9c17e9274cfe9fefe3547a881740','西城区大房',1,'梅斯','西城区',290,123,3,9,'西向',2,3,'简易',2,'2','f8b9d2f5db934b2084fac8c9df36e9ca',2,'2021-06-09 22:34:56','2021-06-09/202106092234389565001.jpeg'),('58bec80063684d54bd9b63c703336053','太湖大乘',3,'huba','湖边',220,128,8,12,'无',1,4,' 精装修',2,'881817b4e97642bca8a179d00b0dc038','4f6295e7d6b542d8a81fc6db60968134',2,'2021-06-09 22:43:02','2021-06-09/202106092242506107295.jpeg'),('69fa37b350c54096a1de7417ff729f15','天津朝天门',5,'dalo','朝天门',400,132,15,21,'坐北朝南',1,5,'豪华',2,'2','f8b9d2f5db934b2084fac8c9df36e9ca',1,'2021-06-09 22:33:35','2021-06-09/202106092233061367757.jpeg'),('6bc9c2c2f0494942a01887ee141b4567','上海1号院',4,'1号院','浦东',680,210,19,26,'豪华',2,5,'豪华装修',2,'2','4f6295e7d6b542d8a81fc6db60968134',2,'2021-06-09 22:39:43','2021-06-09/202106092239325497117.jpeg'),('81bf7c093ec64ac98020570454ccd35a','上海大陆',4,'绿地','南京路',8000,125,3,4,'w',NULL,NULL,'五',3,'b16c3ce504d6455aa32ecd4df66d5092','4556c046f8774d55b249b3cba0efff60',1,'2021-06-10 00:21:37','2021-06-10/202106100020521581193.jpeg'),('88597c869708465bb9ba020c8f60cfee','武汉江源',3,'保利大盒','黄鹤楼',5000,108,12,15,'临江',NULL,NULL,'临江江景房',3,'881817b4e97642bca8a179d00b0dc038','865f06eee789444c8e2c3154554b2fc1',2,'2021-06-09 22:47:49','2021-06-09/202106092247343296108.jpeg'),('88990e1b1f71488980458ad0b65341f3','浦东新区观景',4,'保利','浦东',678,200,32,40,'高层',2,5,'高层豪华',1,'881817b4e97642bca8a179d00b0dc038','00000000000000000000000000000000',2,'2021-06-09 23:32:32','2021-06-09/202106092332228254750.jpeg'),('8fea6113e88346c897cf44bd105918ca','武汉',3,'大加','大成',123,90,12,22,'朝西',2,1,'毛坯',2,'f5dfd9e415024448b8e586d8017d235b','865f06eee789444c8e2c3154554b2fc1',1,'2021-06-09 22:49:41','2021-06-09/202106092249252733790.jpeg'),('a0a3c3f632fa414eab2457573d560b70','摩天轮房出租',5,'大利','摩天轮',4000,90,5,12,'景色好',NULL,NULL,'望出去就是风景',3,'881817b4e97642bca8a179d00b0dc038','ebc257ec06224b61bb1be042ea123821',1,'2021-06-09 23:00:46','2021-06-09/202106092340246697655.jpeg'),('b3a117390fed462f8fde6ee0a6217246','北京颐和',1,'大盘','2131',430,230,1,1,'园林',2,2,'大',2,'c1d65bbbd2cd4229be56642e1f5c559c','4556c046f8774d55b249b3cba0efff60',2,'2021-06-10 00:23:47','2021-06-10/202106100023408894630.jpeg'),('c1f96b2df1d341dbb716602e09090f73','taihu大陆',3,'绿地','湖边',4654,65,5,17,'无',NULL,NULL,'无',3,'b16c3ce504d6455aa32ecd4df66d5092','ebc257ec06224b61bb1be042ea123821',2,'2021-06-10 00:37:49','2021-06-10/202106100037183878165.jpeg'),('c529d9c94b614f9c82ae1123307970b1','太湖湖边独栋',3,'湖边','草丛路1号',590,320,1,3,'独栋',2,5,'别墅',1,'881817b4e97642bca8a179d00b0dc038','00000000000000000000000000000000',1,'2021-06-09 23:31:08','2021-06-09/202106092331000025359.jpeg'),('c78c4767f5cb40af97f7e7ae0707a1f1','摩天轮风景房出租',5,'大利','摩天轮',3900,78,6,12,'风景好',NULL,NULL,'标志建筑尽收眼底',3,'881817b4e97642bca8a179d00b0dc038','ebc257ec06224b61bb1be042ea123821',1,'2021-06-09 23:02:02','2021-06-09/202106092301468334311.jpeg'),('cca8984b345a4df6b7abe516f704630a','武汉大江',3,'213号公园','江湖',150,78,1,3,'人流大',1,3,'临街',2,'f5dfd9e415024448b8e586d8017d235b','865f06eee789444c8e2c3154554b2fc1',1,'2021-06-09 22:51:06','2021-06-09/202106092250510286985.jpeg'),('ff966e96f1594850b9e09b064b535501','21号公寓',3,'大利空','湖边',170,90,8,19,'简单',2,2,'两房',2,'c1d65bbbd2cd4229be56642e1f5c559c','ebc257ec06224b61bb1be042ea123821',2,'2021-06-10 00:27:06','2021-06-10/202106100026154743897.jpeg');

/*Table structure for table `news` */

DROP TABLE IF EXISTS `news`;

CREATE TABLE `news` (
  `n_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `n_title` varchar(50) NOT NULL COMMENT '标题',
  `n_acter` varchar(50) NOT NULL COMMENT '作者',
  `n_content` text COMMENT '内容',
  `n_time` datetime NOT NULL COMMENT '发布时间',
  `n_available` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否可用',
  `n_image` varchar(50) NOT NULL COMMENT '图片地址',
  PRIMARY KEY (`n_id`),
  KEY `FK_acter_sys_user` (`n_acter`),
  CONSTRAINT `FK_acter_sys_user` FOREIGN KEY (`n_acter`) REFERENCES `sys_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `news` */

insert  into `news`(`n_id`,`n_title`,`n_acter`,`n_content`,`n_time`,`n_available`,`n_image`) values (1,'好房大卖','00000000000000000000000000000000','好房大卖','2021-06-02 21:13:19',1,'2021-06-03/202106032134441278633.jpg'),(3,'大湾区大好房源','00000000000000000000000000000000','大湾区大好房源','2021-06-02 23:00:16',1,'2021-06-03/202106032134524898464.jpg'),(4,'上海之都','00000000000000000000000000000000','上海之都新房开售','2021-06-02 23:52:55',1,'2021-06-03/202106032134586208083.jpg');

/*Table structure for table `order` */

DROP TABLE IF EXISTS `order`;

CREATE TABLE `order` (
  `o_id` varchar(50) NOT NULL COMMENT 'id',
  `o_money` double DEFAULT NULL COMMENT '金额',
  `o_commission` double DEFAULT NULL COMMENT '提成',
  `o_time` datetime DEFAULT NULL COMMENT '交易时间',
  `o_buyer` varchar(50) DEFAULT NULL COMMENT '买家',
  `o_status` tinyint(4) DEFAULT '1' COMMENT '状态',
  `o_house` varchar(50) DEFAULT NULL COMMENT '房屋id',
  PRIMARY KEY (`o_id`),
  KEY `FK_ohouse_house` (`o_house`),
  KEY `FK_buyer_sys_user` (`o_buyer`),
  CONSTRAINT `FK_buyer_sys_user` FOREIGN KEY (`o_buyer`) REFERENCES `sys_user` (`id`),
  CONSTRAINT `FK_ohouse_house` FOREIGN KEY (`o_house`) REFERENCES `houses` (`h_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `order` */

insert  into `order`(`o_id`,`o_money`,`o_commission`,`o_time`,`o_buyer`,`o_status`,`o_house`) values ('047577750bd54051ac98ae93278f09ac',680,68000,'2021-06-09 23:41:06','e5da420263d94f5c8581b494d3f2e998',1,'6bc9c2c2f0494942a01887ee141b4567'),('2a92b4334f9a4cbf85230011240a85bc',220,22000,'2021-06-09 23:41:32','f8b9d2f5db934b2084fac8c9df36e9ca',2,'58bec80063684d54bd9b63c703336053'),('5d3ea21a1a1f45738bb80d64c51a9870',678,67800,'2021-06-09 23:40:50','e5da420263d94f5c8581b494d3f2e998',2,'88990e1b1f71488980458ad0b65341f3'),('86efeb9b3c994b83b82a73ba0b552f46',10000,250,'2021-06-09 23:41:59','4f6295e7d6b542d8a81fc6db60968134',2,'88597c869708465bb9ba020c8f60cfee'),('99c6c12efc1e473d85e8ae45474644a5',4654,232.7,'2021-06-10 01:07:16','e5da420263d94f5c8581b494d3f2e998',2,'c1f96b2df1d341dbb716602e09090f73'),('a5c563f31fc541a29da33c4c697930fc',300,30000,'2021-06-09 23:25:12','4556c046f8774d55b249b3cba0efff60',3,'0c1226ca592a4ae08b7b24fc05056a9c'),('baae1d0f2b2e4fe78eb3e4c3e76f0c2c',170,17000,'2021-06-10 00:39:16','4f6295e7d6b542d8a81fc6db60968134',1,'ff966e96f1594850b9e09b064b535501'),('c29f504c6bfe407fb5b979d8276b1e43',210,21000,'2021-06-09 23:42:53','865f06eee789444c8e2c3154554b2fc1',1,'03a7801717634091b477faf64dcbe0f3'),('d75ad357010e4d1faff6380a52bdb833',430,43000,'2021-06-10 00:35:56','ebc257ec06224b61bb1be042ea123821',2,'b3a117390fed462f8fde6ee0a6217246'),('e4dce1eebcc64c1d946a67952cda2378',310,31000,'2021-06-09 23:43:21','865f06eee789444c8e2c3154554b2fc1',1,'0ab90cabb7a74429a7009b4caee5fff0'),('f0c4f762680f4fa9a9bee838d4e41187',290,29000,'2021-06-09 23:43:53','4556c046f8774d55b249b3cba0efff60',2,'329d9c17e9274cfe9fefe3547a881740');

/*Table structure for table `persistent_logins` */

DROP TABLE IF EXISTS `persistent_logins`;

CREATE TABLE `persistent_logins` (
  `username` varchar(64) NOT NULL,
  `series` varchar(64) NOT NULL,
  `token` varchar(64) NOT NULL,
  `last_used` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`series`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `persistent_logins` */

insert  into `persistent_logins`(`username`,`series`,`token`,`last_used`) values ('zhangsan','/y8KXokDuJpwkZNoTbJeXA==','u1wzZGDQHsZ7NHrfS9K7fQ==','2021-06-09 22:14:20'),('000001','0x0XpOEdkavJ1SEjB+pPMQ==','Yg3CNY5a0PSPr7fMfkTzhw==','2021-06-10 01:14:56'),('000007','27fica9fUspTtHj2Y9fmrQ==','zEaZcOT7GRTRWh5FGl8BlQ==','2021-06-10 00:38:13'),('admin','2LxBNz88M3cYjOSApbtSAQ==','QZRXC5+m/9dD0GNEE16OqQ==','2021-06-10 01:15:21'),('000006','2tGeIQxS4X9BxREsqId3Kw==','t4POAG3YwW+Pxn9bcHVpow==','2021-06-09 22:46:03'),('000003','5ZMR6qkJdKKfMrjSjNupMA==','hwwNCfSA+0AtaBunnVi2aA==','2021-06-09 22:43:45'),('000003','6C7tSXO3gJ9/47t1hXV1+Q==','W9Ct27QI80/C5me2eoDQoQ==','2021-06-10 00:41:58'),('000007','Ap8maqsfIpIyw3tjwFa+TA==','JeVHOsB21UwGod73vMaTiQ==','2021-06-10 00:32:16'),('zhangsan','b0KvK15fMsc7DCTotVLEow==','Wgv1+A8v7s/KXTrE1UzWfA==','2021-06-09 22:17:33'),('zhangsan','DmVbkZXnQV/9nqft2dzrNQ==','q10RcFhQ7I4G32gDPBFD3w==','2021-06-09 22:14:29'),('000001','hSPNvVrhEkNSTIWHh0NEzw==','KIBRsC9zbCyeJiCkRQlviQ==','2021-06-09 22:29:11'),('000007','KyqK7iITpSX9ioMXnpVeOg==','bKqllq/lDGN9FbREsmT0xg==','2021-06-09 22:52:44'),('zhangsan','MmQiivhu+zMxyjVPdh33zQ==','+Lyq7XSawqHqU1tyfriKkA==','2021-06-10 01:04:33'),('000002','MTIyj9Y6OFrmfeJmGzlGGg==','Ydo+6UPdq9byTAwj+MtJOw==','2021-06-09 23:41:36'),('000008','o1XsafMsVz63U5u7dqKrBQ==','vF+mT8sMf757Dvrdn8jlyQ==','2021-06-09 23:25:29'),('000001','pMm1Z7SxIMr5afOf5zkxiQ==','ZSYJwAqkAZZOuluwEGTkhg==','2021-06-09 23:41:07'),('000006','qhWJFYTHeNpjoNzuOT86fw==','8qnGONVOagxOsnGKFs0mpA==','2021-06-09 23:43:29'),('zhangsan','RgqEOujhATMT2UZypgY+cA==','plepCzUwR7wdC8GF8vxlQg==','2021-06-09 22:11:39'),('000008','TLXLLeEyk0lw5rjO+R3LBQ==','Irjh6H2E9uQb+IfNtMI6Hw==','2021-06-09 23:46:07'),('ceshi','UmgYdxyMJ2OYGFEn2ahIWQ==','9jXb1uqWpRAN7j4tyvJChw==','2021-06-04 02:37:12'),('000003','UQkCfUZ2Gdq3CvpB54VhFw==','KEZzDoqTngaccv4yu9bixg==','2021-06-09 23:42:08'),('000002','wEncx1tu7G/TYfuupk8sOA==','uZi3qVLhdhV6q30sNEQOPg==','2021-06-09 22:31:55');

/*Table structure for table `sale` */

DROP TABLE IF EXISTS `sale`;

CREATE TABLE `sale` (
  `s_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `s_name` varchar(50) NOT NULL COMMENT '类别名称',
  `s_available` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否可见',
  PRIMARY KEY (`s_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `sale` */

insert  into `sale`(`s_id`,`s_name`,`s_available`) values (1,'商业住宅',1),(2,'普通住宅',1),(3,'郊区别墅',1);

/*Table structure for table `sys_permission` */

DROP TABLE IF EXISTS `sys_permission`;

CREATE TABLE `sys_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `pid` int(11) NOT NULL COMMENT '对应的父id',
  `type` tinyint(4) DEFAULT NULL COMMENT '权限类型（1是menu，2是permission）',
  `title` varchar(30) DEFAULT NULL COMMENT '名称',
  `percode` varchar(50) DEFAULT NULL COMMENT '权限编码[只有type=1才有  user:view]',
  `icon` varchar(20) DEFAULT NULL COMMENT '图标',
  `href` varchar(150) DEFAULT NULL COMMENT 'url地址',
  `target` varchar(30) DEFAULT NULL,
  `open` tinyint(4) DEFAULT '0' COMMENT '是否展开(1展开0不展开)',
  `available` tinyint(4) DEFAULT '1' COMMENT '是否可用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

/*Data for the table `sys_permission` */

insert  into `sys_permission`(`id`,`pid`,`type`,`title`,`percode`,`icon`,`href`,`target`,`open`,`available`) values (1,0,1,'房屋中介系统',NULL,'&#xe68e;','/admin','',1,1),(2,1,1,'房屋管理',NULL,'&#xe712;',NULL,NULL,0,1),(3,1,1,'房源配置',NULL,'&#xe701;',NULL,NULL,0,1),(4,1,1,'用户管理',NULL,'&#xe770;',NULL,NULL,0,1),(5,1,1,'委托管理',NULL,'&#xe709;',NULL,NULL,0,1),(6,1,1,'系统管理',NULL,'&#xe703;',NULL,NULL,0,1),(7,1,1,'订单管理',NULL,'&#xe719;','/admin/order/toOrder','',0,1),(8,1,1,'新闻管理',NULL,'&#xe710;','/admin/news/toNews','',0,1),(9,2,1,'新房管理',NULL,'&#xe713;','/admin/newHouse/toNewHouseManager','',0,1),(10,2,1,'二手房管理',NULL,'&#xe717;','/admin/newHouse/toSecondHouseManager','',0,1),(11,2,1,'租房管理',NULL,'&#xe718;','/admin/newHouse/toRentManager','',0,1),(12,3,1,'城市管理',NULL,'&#xe700;','/admin/city/toCityManager',NULL,0,1),(13,3,1,'装修风格',NULL,'&#xe704;','/admin/decoration/toDecoration','',0,1),(14,3,1,'出售出租类别',NULL,'&#xe706;','/admin/sale/toSale',NULL,0,1),(15,4,1,'员工管理',NULL,'&#xe708;','/admin/user/staffList','',0,1),(16,4,1,'客源管理',NULL,'&#xe707;','/admin/user/customerList',NULL,0,1),(17,5,1,'出售委托',NULL,'&#xe717;','/admin/entrust/toSellEntrust','',0,1),(18,5,1,'出租委托',NULL,'&#xe718;','/admin/entrust/toLeaseEntrust','',0,1),(19,6,1,'菜单管理',NULL,'&#xe653;','/admin/menu/toMenuManager',NULL,0,1),(20,6,1,'权限管理',NULL,'&#xe720;','/admin/permission/toPermissionManager',NULL,0,1),(21,6,1,'角色管理',NULL,'&#xe722;','/admin/role/toRoleManager','',0,1),(22,6,1,'数据源监控',NULL,'&#xe711;','/druid',NULL,0,1),(23,1,2,'权限：首页','main:main',NULL,'/admin/main',NULL,0,1),(24,1,2,'左侧导航栏','admin:left',NULL,'/admin/menu/loadIndexLeftMenuTreeJson',NULL,0,0),(25,19,2,'添加菜单','menu:add',NULL,'/admin/menu/addMenu',NULL,0,1),(26,19,2,'修改菜单','menu:update',NULL,'/admin/menu/updateMenu',NULL,0,1),(27,19,2,'菜单删除','menu:delete',NULL,'/admin/menu/deleteMenu',NULL,0,1),(28,20,2,'添加权限','permission:add',NULL,'/admin/permission/addPermission',NULL,0,1),(29,20,2,'修改权限','permission:update',NULL,'/admin/permission/updatePermission',NULL,0,1),(30,20,2,'删除权限','permission:delete',NULL,'/admin/permission/deletePermission',NULL,0,1),(31,21,2,'添加角色','role:add',NULL,'/admin/role/addRole',NULL,0,1),(32,21,2,'角色修改','role:update',NULL,'/admin/role/updateRole',NULL,0,1),(33,21,2,'删除角色','role:delete',NULL,'/admin/role/deleteRole',NULL,0,1),(34,21,2,'分配角色','role:save',NULL,'/admin/role/saveRolePermission',NULL,0,1),(35,15,2,'添加员工','staff:add',NULL,'/admin/user/addStaff',NULL,0,1),(36,15,2,'修改员工','staff:update',NULL,'/admin/user/updateStaff',NULL,0,1),(37,15,2,'分配角色','staff:save',NULL,'/admin/user/saveStaffRole',NULL,0,1),(38,15,2,'重置员工密码','staff:reset',NULL,'/admin/user/resetPwd',NULL,0,1),(39,16,2,'禁用和解除禁用','customer:ban',NULL,'/admin/user/banCustomer',NULL,0,1),(40,12,2,'添加城市','city:add',NULL,'/admin/city/addCity',NULL,0,1),(41,12,2,'修改城市','city:update',NULL,'/admin/city/updateCity',NULL,0,1),(42,12,2,'城市是否可用','city:delete',NULL,'/admin/city/deleteCity',NULL,0,1),(43,13,2,'添加装修风格','decoration:add',NULL,'/admin/decoration/addDecoration',NULL,0,1),(44,13,2,'修改装修风格','decoration:update',NULL,'/admin/decoration/updateDecoration',NULL,0,1),(45,13,2,'装修风格是否可用','decoration:delete',NULL,'/admin/decoration/deleteDecoration',NULL,0,1),(46,14,2,'添加类别','sale:add',NULL,'/admin/sale/addSale',NULL,0,1),(47,14,2,'修改类别','sale:update',NULL,'/admin/sale/updateSale',NULL,0,1),(48,14,2,'类别是否可用','sale:delete',NULL,'/admin/sale/deleteSale',NULL,0,1),(49,2,2,'添加房屋','house:add',NULL,'/admin/newHouse/addHouse',NULL,0,1),(50,2,2,'修改房屋','house:update',NULL,'/admin/newHouse/updateHouse',NULL,0,1),(51,2,2,'取消房屋','house:cancel',NULL,'/admin/newHouse/cancelHouse',NULL,0,1),(52,5,2,'取消委托','entrust:delete',NULL,'/admin/entrust/deleteEntrust',NULL,0,1),(53,8,2,'添加新闻','new:add',NULL,'/admin/news/addNews',NULL,0,1),(54,8,2,'修改新闻','new:update',NULL,'/admin/news/updateNews',NULL,0,1),(55,8,2,'删除新闻','new:delete',NULL,'/admin/news/deleteNews',NULL,0,1),(56,7,2,'取消订单','order:cancel',NULL,'/admin/order/cancelOrder',NULL,0,1),(57,1,1,'数据分析',NULL,'&#xe730;','','',0,1),(58,57,1,'城市员工年度销售额',NULL,'','/admin/analysis/toCityStaffAnalysis','',0,1),(59,57,1,'城市年度销售额',NULL,'','/admin/analysis/toCityAnalysis','',0,1),(60,57,1,'员工每月销售额',NULL,'','/admin/analysis/toStaffAnalysis','',0,1),(61,57,1,'其他信息',NULL,'','/admin/analysis/toOtherAnalysis','',0,1);

/*Table structure for table `sys_role` */

DROP TABLE IF EXISTS `sys_role`;

CREATE TABLE `sys_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(30) NOT NULL COMMENT '名字',
  `nameZh` varchar(30) DEFAULT NULL COMMENT '中文名',
  `remark` varchar(30) DEFAULT NULL COMMENT '描述',
  `available` tinyint(11) NOT NULL DEFAULT '1' COMMENT '是否可用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `sys_role` */

insert  into `sys_role`(`id`,`name`,`nameZh`,`remark`,`available`) values (1,'ROLE_ADMIN','超级管理员','拥有所有权限',1),(2,'ROLE_MANAGER','经理','出系统管理权限外都有',1),(3,'ROLE_HOPOWNER','店长','能管理门店员工，管理门店房源，管理门店订单等',1),(4,'ROLE_CLERK','店员','',1),(5,'ROLE_CESHI','测试角色','',1);

/*Table structure for table `sys_role_permission` */

DROP TABLE IF EXISTS `sys_role_permission`;

CREATE TABLE `sys_role_permission` (
  `rid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  PRIMARY KEY (`rid`,`pid`),
  KEY `sys_role_permission_ibfk_1` (`pid`),
  CONSTRAINT `sys_role_permission_ibfk_1` FOREIGN KEY (`pid`) REFERENCES `sys_permission` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sys_role_permission_ibfk_2` FOREIGN KEY (`rid`) REFERENCES `sys_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_role_permission` */

insert  into `sys_role_permission`(`rid`,`pid`) values (1,1),(2,1),(3,1),(4,1),(5,1),(1,2),(2,2),(3,2),(4,2),(5,2),(1,3),(2,3),(1,4),(2,4),(3,4),(4,4),(5,4),(1,5),(2,5),(3,5),(4,5),(5,5),(1,6),(5,6),(1,7),(2,7),(3,7),(4,7),(1,8),(2,8),(1,9),(2,9),(3,9),(4,9),(5,9),(1,10),(2,10),(3,10),(4,10),(5,10),(1,11),(2,11),(3,11),(4,11),(5,11),(1,12),(2,12),(1,13),(2,13),(1,14),(2,14),(1,15),(2,15),(3,15),(5,15),(1,16),(2,16),(3,16),(4,16),(5,16),(1,17),(2,17),(3,17),(4,17),(1,18),(2,18),(3,18),(4,18),(5,18),(1,19),(5,19),(1,20),(5,20),(1,21),(5,21),(1,22),(1,23),(2,23),(3,23),(4,23),(5,23),(1,25),(1,26),(5,26),(1,27),(1,28),(1,29),(5,29),(1,30),(5,30),(1,31),(5,31),(1,32),(1,33),(1,34),(1,35),(2,35),(3,35),(5,35),(1,36),(2,36),(3,36),(5,36),(1,37),(2,37),(5,37),(1,38),(2,38),(3,38),(5,38),(1,39),(2,39),(3,39),(4,39),(1,40),(2,40),(1,41),(2,41),(1,42),(2,42),(1,43),(2,43),(1,44),(2,44),(1,45),(2,45),(1,46),(2,46),(1,47),(2,47),(1,48),(2,48),(1,49),(2,49),(3,49),(4,49),(1,50),(2,50),(3,50),(4,50),(1,51),(2,51),(3,51),(4,51),(1,52),(2,52),(3,52),(4,52),(1,53),(2,53),(1,54),(2,54),(1,55),(2,55),(1,56),(2,56),(3,56),(4,56),(1,57),(2,57),(3,57),(1,58),(2,58),(3,58),(1,59),(2,59),(3,59),(1,60),(2,60),(3,60),(1,61),(2,61),(3,61);

/*Table structure for table `sys_role_user` */

DROP TABLE IF EXISTS `sys_role_user`;

CREATE TABLE `sys_role_user` (
  `rid` int(11) NOT NULL,
  `uid` varchar(50) NOT NULL,
  PRIMARY KEY (`rid`,`uid`),
  KEY `sys_role_user_ibfk_2` (`uid`),
  CONSTRAINT `sys_role_user_ibfk_1` FOREIGN KEY (`rid`) REFERENCES `sys_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sys_role_user_ibfk_2` FOREIGN KEY (`uid`) REFERENCES `sys_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_role_user` */

insert  into `sys_role_user`(`rid`,`uid`) values (1,'00000000000000000000000000000000'),(2,'0c14e338a42646918967e7ce715bc459'),(5,'13478ac4c6b94d8c816e414726a57ecf'),(3,'2'),(4,'881817b4e97642bca8a179d00b0dc038'),(4,'b16c3ce504d6455aa32ecd4df66d5092'),(4,'c1d65bbbd2cd4229be56642e1f5c559c'),(4,'f5dfd9e415024448b8e586d8017d235b');

/*Table structure for table `sys_user` */

DROP TABLE IF EXISTS `sys_user`;

CREATE TABLE `sys_user` (
  `id` varchar(50) NOT NULL COMMENT 'id',
  `username` varchar(30) NOT NULL COMMENT '账号名',
  `password` varchar(100) NOT NULL COMMENT '密码',
  `name` varchar(50) NOT NULL COMMENT '姓名',
  `city` int(11) DEFAULT NULL COMMENT '所属城市',
  `sex` tinyint(4) NOT NULL COMMENT '性别(1男0女）',
  `phone` varchar(20) NOT NULL COMMENT '手机号',
  `identity` varchar(20) NOT NULL COMMENT '身份证',
  `position` varchar(50) DEFAULT NULL COMMENT '职位',
  `available` tinyint(4) NOT NULL DEFAULT '1' COMMENT '是否可用',
  `type` tinyint(4) NOT NULL COMMENT '身份（1员工，0顾客）',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sys_user` */

insert  into `sys_user`(`id`,`username`,`password`,`name`,`city`,`sex`,`phone`,`identity`,`position`,`available`,`type`) values ('00000000000000000000000000000000','admin','$2a$10$k7e0OHIRSfRKPHNdfAN1h.wybbEQsBCqECN8vFiy8P0uHZRD01V5y','超级管理员',1,1,'18000000000','440683111111111111','超级管理员',1,1),('0c14e338a42646918967e7ce715bc459','zl','$2a$10$8JKrq7WpU6z2RRL920vek.osFYLva3ZsU1yjQZRyZ.Ouy31YEBgxK','赵六',3,1,'18000000000','444444400011111111','经理',1,1),('13478ac4c6b94d8c816e414726a57ecf','ceshi','$2a$10$e6QYe0kpohzWCMQourrRZeUO3zTlt8lLBm.FvmMGChyqpHuoENb4O','测试账号',1,1,'18000000000','440683199910134718','测试',1,1),('2','ls','$2a$10$k7e0OHIRSfRKPHNdfAN1h.wybbEQsBCqECN8vFiy8P0uHZRD01V5y','李四',2,0,'18000000000','440683111111111112','店长',1,1),('3','zs','$2a$10$k7e0OHIRSfRKPHNdfAN1h.wybbEQsBCqECN8vFiy8P0uHZRD01V5y','张三',NULL,0,'18000000000','440683111111111111',NULL,1,0),('4556c046f8774d55b249b3cba0efff60','000008','$2a$10$SinTjbHmVPhHgWwEWiG4XOkrFDEQLTplTsIH9amN04Ny2bw9OsW/W','大热地方',NULL,1,'21321333333','441243365555555555',NULL,1,0),('4f6295e7d6b542d8a81fc6db60968134','000003','$2a$10$6kuvvzTsFArB6KJ3lPEpBurqT.84yDZf62MWJz7CTEoc6NWE5X72q','wsy',NULL,1,'12320000000','441233333333333333',NULL,1,0),('865f06eee789444c8e2c3154554b2fc1','000006','$2a$10$llkYRd11YAttnd8a25wtqOsxCT3nRuOkogiyVSy1TqlKkZIHjUldq','zhengg',NULL,0,'12900000000','436666666666666666',NULL,1,0),('881817b4e97642bca8a179d00b0dc038','kkx','$2a$10$oWPLGsRj7ujVEwxzEFsRcOxMVFsuwN3itNNX3mgEpCHqt5GMQeLP6','可可西',2,1,'18000000000','440682222222222222','员工',1,1),('a4af8e476fe849e2bf6e4920aea281f5','wangwu','$2a$10$0RjSC0qCeHsgANlxV49OF.hv8xFS3q1Wfterw.Ix4s72fI2ipUT7.','123',NULL,1,'12322222222','123222222222222222',NULL,1,0),('b16c3ce504d6455aa32ecd4df66d5092','cat','$2a$10$Yw.RI1YV.JArBLCky2Agnej8Wrkgt1OvTL6PO1rIIXY4W8sNG14k.','阿猫',1,1,'12376352837','337281736548237261','员工',1,1),('c1d65bbbd2cd4229be56642e1f5c559c','dog','$2a$10$GMsjTjl9JBWsJD6Y5Ysyh.ltNAQzeuudO1kHyRAFQAn3jm4g75vHu','阿狗',1,1,'13697756273','441823213413428476','员工',1,1),('e5da420263d94f5c8581b494d3f2e998','000001','$2a$10$.pMCjhc.cfFQFiv27om3FeEeqw0SiqY4kvDaNKVlyYZR14QxI2zWa','yh',NULL,0,'18000000000','400000000000000000',NULL,1,0),('ebc257ec06224b61bb1be042ea123821','000007','$2a$10$OTX.0mUFoG13l7M5KXRN6.T8mPhQa0IAS0YQBMEumAVbxgwtDOxFK','潮人',NULL,1,'12377777777','123143434343434343',NULL,1,0),('ed2ed0fcb2ee4cd09a98b8666293f79a','zhangsan','$2a$10$3cYPxVaMFbmHYSkaHXzLlexCPDmjBGGTOTDWmvk8ngjnwGRXd6Ise','张三',NULL,1,'18000000000','440000000000000000',NULL,1,0),('f5dfd9e415024448b8e586d8017d235b','ww','$2a$10$L5jMvLcZtzbnVxei89DKDOnfOsy4E902.L.Jk5blv/QPOKbbaxMpO','王五',3,0,'18000000000','444444444444444444','员工',1,1),('f8b9d2f5db934b2084fac8c9df36e9ca','000002','$2a$10$d7qd6ZQYI4luKC2h0lB6beTEuKJTus30UPNo5j0ROesNu424pSzPa','lzf',NULL,1,'18200000000','423432441111111111',NULL,1,0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
